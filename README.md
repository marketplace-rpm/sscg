# Information / Информация

SPEC-файл для создания RPM-пакета **sscg**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/sscg`.
2. Установить пакет: `dnf install sscg`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)